const playerQueryResolvers = {
  players : async (_, __, { playersModel }) => {
    try {
      return await playersModel.find({});
    } catch (error) {
      return error.message;
    }
  },
  playerById: async (_, { playerId }, { playersModel }) => {
    try {
      return await playersModel.findById(playerId);
    } catch (error) {
      return error;
    }
  },
};

const playerMutationResolvers = {
  addPlayer: async (_, args, { playersModel }) => {
    try {
      delete args.id
      return await playersModel.create(args);
    } catch (error) {
      return error.message;
    }
  },
  deletePlayer: async (_, args, { playersModel }) => {
    try {
      const resp = await playersModel.findByIdAndDelete({ _id: args.playerId });
      return resp;
    } catch(error) {
      return error.message;
    }
  },
};

 module.exports = {
  playerQueryResolvers,
  playerMutationResolvers
 };
 