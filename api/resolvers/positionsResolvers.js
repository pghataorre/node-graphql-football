const positionQueryResolvers = {
  positions: async (_, __ , { positionsModel }) => {
    try {
      return await positionsModel.find({}); 
    } catch (error) {
      return error.message;
    }
  },
};

const managerMutationResolvers = {}

module.exports = {
  positionQueryResolvers
};
