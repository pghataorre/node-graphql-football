const { 
  managerQueryResolvers,
  managerMutationResolvers
} = require('./managersResolvers');

const {
  teamsMutationResolvers,
  teamsQueryResolvers
} = require('./teamsResolvers');

const {
  tournamentsQueryResolvers,
  tournamentsMutationResolvers
} = require('./tournamentsResolver');

const {
  playerQueryResolvers,
  playerMutationResolvers
} = require('./playersResolvers');

const {
  positionQueryResolvers
} = require('./positionsResolvers');

const resolvers = {
  Query: {
    ...managerQueryResolvers,
    ...teamsQueryResolvers,
    ...tournamentsQueryResolvers,
    ...playerQueryResolvers,
    ...positionQueryResolvers
  },

  Mutation: {
    ...managerMutationResolvers,
    ...teamsMutationResolvers,
    ...tournamentsMutationResolvers,
    ...playerMutationResolvers
  },
};

module.exports = resolvers;
