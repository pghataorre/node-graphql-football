const teamsQueryResolvers = {
  teams: async (_, __ , { teamsModel }) => {
    try {
      return await teamsModel.find({}).populate('manager').populate('teamPlayers');
    } catch (error) {
      return error;
    }
  },
  teamById: async (_, { id }, { teamsModel }) => {
    try {
      return await teamsModel.findById(id).populate('manager').populate('teamPlayers');
    } catch (error) {
      return error.message;
    }
  }
};

const teamsMutationResolvers = {
  addTeam: async (_, args, { teamsModel }) => {
    try {
      delete args.id;
      return await teamsModel.create(args);
    } catch (error) {
      return error.message;
    }
  },
  deleteTeam: async (_, args, { teamsModel }) => {
    try {
      const resp = await teamsModel.findByIdAndDelete({ _id: args.id });
      return resp;
    } catch (error) {
      return error.message;
    }
  }
};

module.exports = {
  teamsQueryResolvers,
  teamsMutationResolvers,
}
