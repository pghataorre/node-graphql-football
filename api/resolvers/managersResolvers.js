const managerQueryResolvers = {
  managers: async (_, __ , { managersModel }) => {
    try {
      return await managersModel.find({}); 
    } catch (error) {
      return error.message;
    }
  },
  managerById: async (_, { managerId }, { managersModel }) => {
    try {
      return await managersModel.findById(managerId);
    } catch (error) {
      return error.message;
    }
  }
};

const managerMutationResolvers = {
  addManager: async (_, args, { managersModel }) => {
    delete args.id;
    try {
      return await managersModel.create(args);
    } 
    catch(error) {
      return error.message;
    }
  },
  deleteManager: async (_, args, { managersModel }) => {
    try {
      const resp = await teamsModel.findByIdAndDelete({ _id: args.id });
      return resp;
    } catch(error) {
      return error.message;
    }
  },
}

module.exports = {
  managerQueryResolvers,
  managerMutationResolvers
};
