const tournamentsQueryResolvers = {
  tournaments: async (_, __ , { tournamentsModel }) => {
    try {
      return await tournamentsModel.find({});
    } catch (error) {
      return error.message;
    }
  },
  tournamentById: async (_, { tourId }, { tournamentsModel }) => {
    try {
      return await tournamentsModel.findById(tourId);
    } catch (error) {
      return error.message;
    }
  }
};

const tournamentsMutationResolvers = {
  addTournament: async (_, args, { tournamentsModel }) => {
    delete args.id;
    try {
      return await tournamentsModel.create(args);
    } 
    catch(error) {
      return error;
    }
  },
  deleteTournament: async (_, args, { tournamentsModel }) => {
    try {
      const resp = await tournamentsModel.findByIdAndDelete({ _id: args.id });
      return resp;
    } catch(error) {
      return error.message;
    }
  },
};

module.exports = {
  tournamentsQueryResolvers,
  tournamentsMutationResolvers
};
