const mongoose = require('mongoose');

const positionSchema = new mongoose.Schema({
  positionName: {
    type: String,
    require: true,
  },
  positionKey: {
    type: String,
    require: true,
  },
  positionalArea: {
    type: String,
    require: true,
  }
});

module.exports = positionSchema;
