const mongoose = require('mongoose');

const managersSchema = require('./managersSchema');
const teamsSchema = require('./teamsSchema');
const tournamentsSchema = require('./tournamentsSchema');
const playersSchema = require('./playersSchema');
const positionsSchema = require('./positionsSchema');

const createModel = (collectionName, collectionSchema) => {
  try {
    return mongoose.model(collectionName);
  } catch (e) {
    return mongoose.model(collectionName, collectionSchema);
  }
}

module.exports = {
  teamsModel: createModel('teams', teamsSchema), 
  managersModel: createModel('managers', managersSchema),
  tournamentsModel: createModel('tournaments', tournamentsSchema),
  playersModel: createModel('players', playersSchema),
  positionsModel: createModel('positions', positionsSchema),
};
