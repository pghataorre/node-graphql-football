const mongoose = require('mongoose');

const teamsSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  teamBadge: {
    type: String,
    required: true,
    unique: true
  },
  manager: {
    type: mongoose.Schema.Types.ObjectId,
    // Use of ref allows a future join to the mongo table set in ref you want to join')
    ref: 'managers',
    required: true,
  },
});

teamsSchema.virtual('teamPlayers', {
  ref: 'players',
  localField: '_id',
  foreignField: 'teamId',
});

module.exports = teamsSchema;
