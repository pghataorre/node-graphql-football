const mongoose = require('mongoose');

const playersSchema = new mongoose.Schema({
  firstName: {
    type: String,
    require: true,
  },
  lastName: {
    type: String,
    require: true,
  },
  shirtNumber: {
    type: String,
    required: true,
  },
  position: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'positions',
    required: true,
  }],
  teamId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'teams',
    required: true,
  }],
  playerImage: {
    type: String,
  },
  playerDescription: {
    type: String,
  },
});

module.exports = playersSchema;
