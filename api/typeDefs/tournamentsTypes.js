const tournamentsTypes = `
  type Tournaments {
    id: ID!
    name: String!
    tournamentLogo: String
    description: String
    tournamentTeams: [String!]
  }
`;

const tournamentsQueryTypes = `
  tournaments: [Tournaments]
  tournamentById(tourId: ID!): Tournaments!
`;

const tournamentsMutations = `
  addTournament(
    id: ID!
    name: String!
    tournamentLogo: String
    description: String
    tournamentTeams: [String!]
  ): Tournaments

  deleteTournament(
    id: ID!
  ): Tournaments!
`;

module.exports = {
  tournamentsTypes,
  tournamentsQueryTypes,
  tournamentsMutations
};
