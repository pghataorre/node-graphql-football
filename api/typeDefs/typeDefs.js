const { gql } = require('apollo-server');
const {
  managerTypes,
  managerQueryTypes,
  managerMutations
} = require('./managersTypes');

const {
  teamsTypes,
  teamsQueryTypes,
  teamsMutations
} = require('./teamsTypes');

const {
  tournamentsTypes,
  tournamentsQueryTypes,
  tournamentsMutations
} = require('./tournamentsTypes');

const {
  playerTypes,
  playerQueryTypes,
  playerMutations
} = require('./playersTypes')

const { 
  positionTypes,
  positionQueryTypes
} = require('./positionTypes');

const allDefs = [playerTypes,managerTypes, teamsTypes, tournamentsTypes, positionTypes];
const allQueryTypes = [playerQueryTypes, managerQueryTypes, teamsQueryTypes, tournamentsQueryTypes, positionQueryTypes]
const allMutations = [playerMutations, managerMutations, teamsMutations, tournamentsMutations];

const typeDefs = gql`
  ## --------------------------------
  ## Types
  ## --------------------------------
  ${allDefs.join('\n')}

  ## --------------------------------
  ## Queries
  ## --------------------------------
  type Query {
    ${allQueryTypes.join('\n')}
  }

  ## --------------------------------
  ## inputSchemas
  ## --------------------------------

  # CANNOT GET IT WORKING WITH THE INPUT BELOW PLEASE SUGGEST HOW TO FIX
  # input newManager {
  #   id: ID!
  #   name: String!
  # }

  ## --------------------------------
  ## Mutations
  ## --------------------------------
  type Mutation {
    ${allMutations.join('\n')}
  }
`;

module.exports = typeDefs;
