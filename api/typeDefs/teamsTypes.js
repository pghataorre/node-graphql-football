const teamsTypes = `
  type Teams {
    id: ID!
    name: String!
    teamBadge: String!
    manager: Manager
    teamPlayers: [Players]
  } 
`;

const teamsQueryTypes = `
  teams: [Teams]
  teamById(id: ID!): Teams
`;

const teamsMutations = `
  addTeam(
    id: ID!
    name: String!
    teamBadge: String!
    manager: ID!
  ): Teams!

  deleteTeam(
    id: ID!
  ): Teams!
`;

module.exports = {
  teamsTypes,
  teamsQueryTypes,
  teamsMutations
}