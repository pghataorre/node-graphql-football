const positionTypes = `
  type Position {
    id: ID!,
    positionName: String!
    positionKey: String!
    positionalArea: String!
  }
`;

const positionQueryTypes = `
  positions: [Position]
`;

module.exports  = {
  positionTypes,
  positionQueryTypes
}
