const managerTypes = `
  type Manager {
    id: ID
    name: String
  }
`;

const managerQueryTypes = `
  managers: [Manager]
  managerById(managerId: ID!): Manager
`;

const managerMutations = `
  addManager(
    id: ID!,
    name: String!
  ): Manager!

  deleteManager(
    id: ID!
  ): Manager!
`;

module.exports = {
  managerTypes,
  managerQueryTypes,
  managerMutations,
};
