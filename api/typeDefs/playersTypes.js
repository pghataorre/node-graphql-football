const playerTypes = `
  type Players {
    id: ID!
    firstName: String! 
    lastName: String!
    shirtNumber: Int!
    position: [String!]
    teamId: [String!]
    playerImage: String
    playerDescription: String
  }
`;

const playerQueryTypes = `
  players: [Players]
  playerById(playerId: ID!): Players!
`;

const playerMutations = `
  addPlayer(
    id: ID!
    firstName: String! 
    lastName: String!
    shirtNumber: Int!
    position: [String!]
    teamId: [String!]
    playerImage: String
    playerDescription: String
  ): Players!

  deletePlayer(
    playerId: ID!
  ): Players!
`;

module.exports = {
  playerTypes,
  playerQueryTypes,
  playerMutations
}

