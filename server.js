const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const resolvers = require('./api/resolvers/resolvers');
const typeDefs = require('./api/typeDefs/typeDefs');

mongoose.connect('mongodb://root:example@localhost/teams?authSource=admin&w=1', { useNewUrlParser: true, useUnifiedTopology: true });

const { 
  managersModel,
  teamsModel,
  tournamentsModel,
  playersModel,
  positionsModel
} = require('./api/models/models');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context() {
    return {
      managersModel,
      teamsModel,
      tournamentsModel,
      playersModel,
      positionsModel
    }
  }
});

server.listen(process.env.PORT || 5000).then(({url}) => console.log(`\n *********************\n server running on ${url} \n *********************\n`));
